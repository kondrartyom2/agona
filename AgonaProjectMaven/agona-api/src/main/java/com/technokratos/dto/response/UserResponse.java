package com.technokratos.dto.response;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class UserResponse {

    private String firstName;

    private String lastName;

    private String username;
}
