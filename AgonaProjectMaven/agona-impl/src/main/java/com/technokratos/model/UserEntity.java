package com.technokratos.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Пользователь
 */
@Entity
@Getter
@Setter
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "account")
public class UserEntity extends AbstractEntity {

    /** Логин */
    @Column(unique = true, nullable = false, updatable = false)
    private String username;

    /** Захешированный пароль */
    @Column(name = "hash_password", nullable = false)
    private String hashPassword;

    /** Имя */
    @Column(name = "first_name")
    private String firstName;

    /** Фамилия */
    @Column(name = "last_name")
    private String lastName;
}
