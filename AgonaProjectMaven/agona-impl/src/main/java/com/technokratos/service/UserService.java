package com.technokratos.service;

import com.technokratos.dto.request.*;
import com.technokratos.dto.response.*;

import java.util.UUID;

public interface UserService {

    UUID createUser(UserExtendedRequest user);

    UserResponse getUserById(UUID userId);
}
