package com.technokratos.service.impl;

import com.technokratos.dto.request.*;
import com.technokratos.dto.response.*;
import com.technokratos.exception.UserNotFoundException;
import com.technokratos.repository.UserRepository;
import com.technokratos.service.UserService;
import com.technokratos.utils.mapper.UserMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;

    @Override
    public UUID createUser(UserExtendedRequest user) {
        return userRepository.save(userMapper.toEntity(user)).getUuid();
    }

    @Override
    public UserResponse getUserById(UUID userId) {
        return userMapper.toResponse(
                userRepository.findById(userId).orElseThrow(UserNotFoundException::new)
        );
    }
}
