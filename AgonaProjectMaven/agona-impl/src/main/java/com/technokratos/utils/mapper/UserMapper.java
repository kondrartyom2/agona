package com.technokratos.utils.mapper;

import com.technokratos.dto.request.UserExtendedRequest;
import com.technokratos.dto.response.UserResponse;
import com.technokratos.model.UserEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface UserMapper {

    @Mapping(target = "uuid", ignore = true)
    @Mapping(target = "createDate", ignore = true)
    @Mapping(target = "hashPassword", source = "password")
    UserEntity toEntity(UserExtendedRequest userRequest);

    UserResponse toResponse(UserEntity userEntity);
}
