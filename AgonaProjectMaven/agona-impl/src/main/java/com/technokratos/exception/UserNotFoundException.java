package com.technokratos.exception;

public class UserNotFoundException extends AgonaNotFoundException {

    public UserNotFoundException() {
        super("User not found");
    }
}
