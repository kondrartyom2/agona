package com.technokratos.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class AgonaServiceException extends RuntimeException {
    private final HttpStatus httpStatus;

    public AgonaServiceException(HttpStatus httpStatus, String message) {
        super(message);
        this.httpStatus = httpStatus;
    }
}
