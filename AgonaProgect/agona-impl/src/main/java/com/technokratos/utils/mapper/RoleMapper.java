package com.technokratos.utils.mapper;

import com.technokratos.dto.response.RoleResponse;
import com.technokratos.model.RoleEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.NullValueCheckStrategy;

import java.util.List;

@Mapper(componentModel = "spring", uses = PrivilegeMapper.class,
        nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface RoleMapper {

    @Mapping(target = "roleDescription", source = "role.description")
    RoleResponse toResponse(RoleEntity role);

    List<RoleResponse> toResponse(List<RoleEntity> roles);
}
