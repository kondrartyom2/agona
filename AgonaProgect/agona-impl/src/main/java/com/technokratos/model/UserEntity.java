package com.technokratos.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.util.Set;

/**
 * Пользователь
 */
@Entity
@Getter
@Setter
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "account")
public class UserEntity extends AbstractEntity {

    /** Логин */
    @Column(unique = true, nullable = false, updatable = false)
    private String username;

    /** Захешированный пароль */
    @Column(name = "hash_password", nullable = false)
    private String hashPassword;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "account_roles",
            joinColumns = {@JoinColumn(name = "account_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "role_id", referencedColumnName = "id")}
    )

    private Set<RoleEntity> roles;

    /** Имя */
    @Column(name = "first_name")
    private String firstName;

    /** Фамилия */
    @Column(name = "last_name")
    private String lastName;
}
