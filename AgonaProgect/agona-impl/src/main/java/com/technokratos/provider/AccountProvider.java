package com.technokratos.provider;

import com.technokratos.service.AccountService;
import com.technokratos.service.jwt.AccountRefreshTokenService;

import java.util.List;

public interface AccountProvider {

    AccountService getAccountService(List<String> roles);

    AccountRefreshTokenService getAccountRefreshTokenService(List<String> roles);
}
