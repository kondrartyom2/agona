package com.technokratos.provider;

import com.technokratos.dto.response.UserResponse;
import com.technokratos.model.RefreshTokenEntity;

import java.util.List;

public interface JwtRefreshTokenProvider {
    String generateRefreshToken(UserResponse accountResponse);

    RefreshTokenEntity verifyRefreshTokenExpiration(String refreshToken, List<String> roles);
}
