package com.technokratos.provider;

import com.technokratos.service.AccountService;
import com.technokratos.service.UserService;
import com.technokratos.service.jwt.AccountRefreshTokenService;
import com.technokratos.service.jwt.UserRefreshTokenService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

import static com.technokratos.dto.enums.Role.USER;

@RequiredArgsConstructor
@Component
public class AccountProviderImpl implements AccountProvider {

    private final UserService userService;
    private final UserRefreshTokenService userRefreshTokenService;

    @Override
    public AccountService getAccountService(List<String> roles) {
        if (roles.contains(USER.name()))
            return userService;
        else
            //For example another service
            return null;
    }

    @Override
    public AccountRefreshTokenService getAccountRefreshTokenService(List<String> roles) {
        if (roles.contains(USER.name()))
            return userRefreshTokenService;
        else
            //For example another service
            return null;
    }
}
