package com.technokratos.controllers.handlers;

import com.technokratos.exception.AgonaServiceException;
import io.jsonwebtoken.JwtException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * Обработчик возникающих ошибок.
 */
@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    /**
     * Формирует ответ на основе всех исключений {@link Exception}.
     *
     * @param exception исключение
     * @return сформированный на основе исключения ответ
     */
    @ExceptionHandler(Exception.class)
    public final ResponseEntity<ExceptionMessage> onAllExceptions(Exception exception) {

        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(ExceptionMessage.builder()
                        .message(exception.getMessage())
                        .exceptionName(exception.getClass().getSimpleName())
                        .build());
    }

    /**
     * Формирует ответ на основе исключений {@link AccessDeniedException}.
     *
     * @param exception исключение
     * @return сформированный на основе исключения ответ
     */
    @ExceptionHandler(AccessDeniedException.class)
    public final ResponseEntity<ExceptionMessage> onAccessDeniedException(AccessDeniedException exception) {

        return ResponseEntity.status(HttpStatus.FORBIDDEN)
                .body(ExceptionMessage.builder()
                        .message(exception.getMessage())
                        .exceptionName(exception.getClass().getSimpleName())
                        .build());
    }

    /**
     * Формирует ответ на основе всех исключений по проекту {@link AgonaServiceException}.
     *
     * @param agonaServiceException исключения по проекту
     * @return сформированный на основе исключения ответ
     */
    @ExceptionHandler(AgonaServiceException.class)
    public final ResponseEntity<ExceptionMessage> onAccountExceptionExceptions(AgonaServiceException agonaServiceException) {

        return ResponseEntity.status(agonaServiceException.getHttpStatus())
                .body(ExceptionMessage.builder()
                        .message(agonaServiceException.getMessage())
                        .exceptionName(agonaServiceException.getClass().getSimpleName())
                        .build());
    }

    /**
     * Формирует ответ на основе всех исключений аутентификации {@link AuthenticationException}.
     *
     * @param authenticationException исключение аутентификации
     * @return сформированный на основе исключения ответ
     */
    @ExceptionHandler(AuthenticationException.class)
    public final ResponseEntity<ExceptionMessage> onAuthenticationExceptions(AuthenticationException authenticationException) {

        return ResponseEntity.status(HttpStatus.UNAUTHORIZED)
                .body(ExceptionMessage.builder()
                        .message(authenticationException.getMessage())
                        .exceptionName(authenticationException.getClass().getSimpleName())
                        .build());
    }

    /**
     * Формирует ответ на основе ошибок связанные с токеном {@link JwtException}.
     *
     * @param jwtException исключение аутентификации
     * @return сформированный на основе исключения ответ
     */
    @ExceptionHandler(JwtException.class)
    public final ResponseEntity<ExceptionMessage> onJwtExceptions(JwtException jwtException) {

        return ResponseEntity.status(HttpStatus.UNAUTHORIZED)
                .body(ExceptionMessage.builder()
                        .message(jwtException.getMessage())
                        .exceptionName(jwtException.getClass().getSimpleName())
                        .build());
    }

}

