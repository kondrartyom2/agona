package com.technokratos.controllers;

import com.technokratos.api.UserApi;
import com.technokratos.dto.response.*;
import com.technokratos.dto.request.*;
import com.technokratos.service.UserService;
import com.technokratos.service.jwt.JwtTokenService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class UserController implements UserApi {

    private final UserService userService;
    private final JwtTokenService tokenService;

    @Override
    public UUID createUser(UserExtendedRequest user) {
        return userService.createUser(user);
    }

    @Override
    public UserResponse getUser(UUID userId) {
        return userService.getUserById(userId);
    }

    @Override
    public void deleteUser(UUID userId) {

    }

    @Override
    public UserResponse updateUser(UUID userId, UserExtendedRequest user) {
        return null;
    }

    @Override
    public TokenCoupleResponse login(UserRequest userRequest) {
        return tokenService.generateTokenCouple(userService.login(userRequest));
    }
}
