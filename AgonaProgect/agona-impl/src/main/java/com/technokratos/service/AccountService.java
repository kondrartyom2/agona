package com.technokratos.service;

import com.technokratos.dto.response.UserResponse;

import java.util.Optional;

public interface AccountService {

    Optional<UserResponse> findBySubject(String subject);
}