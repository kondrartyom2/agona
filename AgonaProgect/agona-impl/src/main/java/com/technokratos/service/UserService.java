package com.technokratos.service;

import com.technokratos.dto.request.*;
import com.technokratos.dto.response.*;

import javax.transaction.Transactional;
import java.util.UUID;

public interface UserService extends AccountService {

    UserResponse login(UserRequest request);

    UUID createUser(UserExtendedRequest user);

    UserResponse getUserById(UUID userId);
}
