package com.technokratos.service.jwt;

import com.technokratos.dto.response.UserResponse;
import com.technokratos.exception.TokenRefreshException;
import com.technokratos.exception.UserNotFoundException;
import com.technokratos.model.RefreshTokenEntity;
import com.technokratos.model.UserRefreshTokenEntity;
import com.technokratos.repository.UserRefreshTokenRepository;
import com.technokratos.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.Instant;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class BaseUserRefreshTokenService implements UserRefreshTokenService {

    @Value("${jwt.expiration.refresh.mills}")
    private long expirationRefreshInMills;

    private final UserRefreshTokenRepository refreshTokenRepository;
    private final UserRepository userRepository;

    @Override
    @Transactional
    public UserRefreshTokenEntity generateRefreshToken(UserResponse accountResponse) {
        return refreshTokenRepository.save(
                UserRefreshTokenEntity.builder()
                        .expiryDate(Instant.now().plusMillis(expirationRefreshInMills))
                        .account(userRepository
                                .findOneByUsername(accountResponse.getUsername())
                                .orElseThrow(UserNotFoundException::new))
                        .build()
        );
    }

    @Override
    public RefreshTokenEntity verifyRefreshTokenExpiryDate(String refreshToken) {
        return refreshTokenRepository.findById(UUID.fromString(refreshToken)).map(token -> {
            refreshTokenRepository.delete(token);
            if (token.getExpiryDate().compareTo(Instant.now()) < 0) {
                throw new TokenRefreshException(String.valueOf(token.getId()), "Срок действия токена обновления истек.");
            }
            return refreshTokenRepository.save(
                    UserRefreshTokenEntity.builder()
                            .expiryDate(Instant.now().plusMillis(expirationRefreshInMills))
                            .account(token.getAccount())
                            .build());
        }).orElseThrow(() -> {
            throw new TokenRefreshException(refreshToken, "Токен не существует.");
        });
    }
}
