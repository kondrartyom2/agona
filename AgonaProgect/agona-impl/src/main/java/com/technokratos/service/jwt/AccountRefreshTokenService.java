package com.technokratos.service.jwt;

import com.technokratos.dto.response.UserResponse;
import com.technokratos.model.RefreshTokenEntity;

public interface AccountRefreshTokenService {

    RefreshTokenEntity generateRefreshToken(UserResponse accountResponse);

    RefreshTokenEntity verifyRefreshTokenExpiryDate(String refreshToken);
}
