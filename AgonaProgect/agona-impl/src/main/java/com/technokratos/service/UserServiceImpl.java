package com.technokratos.service;

import com.technokratos.dto.enums.Privilege;
import com.technokratos.dto.enums.Role;
import com.technokratos.dto.request.*;
import com.technokratos.dto.response.*;
import com.technokratos.exception.AgonaUnauthorizedException;
import com.technokratos.exception.UserNotFoundException;
import com.technokratos.model.PrivilegeEntity;
import com.technokratos.model.RoleEntity;
import com.technokratos.model.UserEntity;
import com.technokratos.repository.PrivilegeRepository;
import com.technokratos.repository.UserRepository;
import com.technokratos.utils.mapper.UserMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collections;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private final PrivilegeRepository privilegeRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    public UserResponse login(UserRequest request) {
        return userRepository.findOneByUsername(request.getUsername())
                .filter(user -> passwordEncoder.matches(request.getPassword(), user.getHashPassword()))
                .map(userMapper::toResponse)
                .orElseThrow(() -> new AgonaUnauthorizedException("Failed to log in: " + request.getUsername()));
    }

    @Override
    public UUID createUser(UserExtendedRequest user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        UserEntity userEntity = userMapper.toEntity(user);
        userEntity.setRoles(Collections.singleton(getRole()));
        return userRepository.save(userEntity).getId();
    }

    @Override
    public UserResponse getUserById(UUID userId) {
        return userMapper.toResponse(
                userRepository.findById(userId).orElseThrow(UserNotFoundException::new)
        );
    }

    @Override
    public Optional<UserResponse> findBySubject(String subject) {
        return userRepository.findOneByUsername(subject)
                .map(userMapper::toResponse);
    }

    private RoleEntity getRole() {
        Set<PrivilegeEntity> privileges = privilegeRepository
                .findByPrivilegeIsIn(Set.of(Privilege.CREATE, Privilege.DELETE, Privilege.WRITE, Privilege.READ));
        return RoleEntity.builder()
                .role(Role.USER)
                .privileges(privileges)
                .build();
    }
}