package com.technokratos.service.jwt;

import com.technokratos.consts.AgonaConstants;
import com.technokratos.dto.TokenCoupleDto;
import com.technokratos.dto.response.RoleResponse;
import com.technokratos.dto.response.TokenCoupleResponse;
import com.technokratos.dto.response.UserResponse;
import com.technokratos.model.RefreshTokenEntity;
import com.technokratos.provider.JwtAccessTokenProvider;
import com.technokratos.provider.JwtRefreshTokenProvider;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static com.technokratos.consts.AgonaConstants.BEARER;


@Service
@RequiredArgsConstructor
public class JwtTokenServiceImpl implements JwtTokenService {

    private final JwtAccessTokenProvider jwtAccessTokenProvider;
    private final JwtRefreshTokenProvider jwtRefreshTokenProvider;

    @Override
    public UserResponse getUserInfoByToken(String token) {
        return jwtAccessTokenProvider.userInfoByToken(token);
    }

    @Override
    public TokenCoupleResponse generateTokenCouple(UserResponse accountResponse) {
        String accessToken = jwtAccessTokenProvider.generateAccessToken(
                accountResponse.getUsername(),
                Collections.singletonMap(AgonaConstants.ROLE, accountResponse
                        .getRoles().stream()
                        .map(RoleResponse::getRole)
                        .collect(Collectors.toList()))
        );
        String refreshToken = jwtRefreshTokenProvider.generateRefreshToken(accountResponse);
        return TokenCoupleResponse.builder()
                .accessToken(BEARER.concat(StringUtils.SPACE).concat(accessToken))
                .refreshToken(refreshToken)
                .accessTokenExpirationDate(jwtAccessTokenProvider.getExpirationDateFromAccessToken(accessToken))
                .build();
    }

    @Override
    public TokenCoupleResponse refreshAccessToken(TokenCoupleDto tokenCoupleDto) {
        List<String> roles = jwtAccessTokenProvider.getRolesFromAccessToken(tokenCoupleDto.getAccessToken().replace(BEARER.concat(StringUtils.SPACE), StringUtils.EMPTY));
        RefreshTokenEntity verifiedRefreshToken = jwtRefreshTokenProvider.verifyRefreshTokenExpiration(
                tokenCoupleDto.getRefreshToken(), roles
        );

        String accessToken = jwtAccessTokenProvider.generateAccessToken(
                jwtAccessTokenProvider.getSubjectFromAccessToken(tokenCoupleDto.getAccessToken().replace(BEARER.concat(StringUtils.SPACE), StringUtils.EMPTY)),
                Collections.singletonMap(AgonaConstants.ROLE, roles));
        return TokenCoupleResponse.builder()
                .refreshToken(String.valueOf(verifiedRefreshToken.getId()))
                .accessToken(BEARER.concat(StringUtils.SPACE).concat(accessToken))
                .accessTokenExpirationDate(jwtAccessTokenProvider.getExpirationDateFromAccessToken(accessToken))
                .build();
    }
}