package com.technokratos.service.jwt;

import com.technokratos.dto.TokenCoupleDto;
import com.technokratos.dto.response.UserResponse;
import com.technokratos.dto.response.TokenCoupleResponse;

public interface JwtTokenService {

    UserResponse getUserInfoByToken(String token);

    TokenCoupleResponse generateTokenCouple(UserResponse accountResponse);

    TokenCoupleResponse refreshAccessToken(TokenCoupleDto tokenCoupleResponse);
}