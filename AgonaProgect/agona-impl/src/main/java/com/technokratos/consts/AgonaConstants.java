package com.technokratos.consts;

public interface AgonaConstants {

    String ROLE = "ROLE";
    String PRIVILEGE = "PRIVILEGE";
    String BEARER = "Bearer";
    String REFRESH_TOKEN = "Refresh-token";
    String ACCESS_TOKEN_EXPIRATION_TIME = "Access-token-expiration-time";
}
