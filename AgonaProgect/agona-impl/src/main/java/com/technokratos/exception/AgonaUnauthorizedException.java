package com.technokratos.exception;

import org.springframework.http.HttpStatus;

public class AgonaUnauthorizedException extends AgonaServiceException {

    public AgonaUnauthorizedException(String message) {
        super(HttpStatus.UNAUTHORIZED, message);
    }
}