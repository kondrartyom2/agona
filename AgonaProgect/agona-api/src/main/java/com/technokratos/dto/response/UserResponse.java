package com.technokratos.dto.response;

import com.technokratos.dto.enums.Role;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.SuperBuilder;

import java.util.List;
import java.util.Set;
import java.util.UUID;

@SuperBuilder
@Data
public class UserResponse {

    private UUID id;

    private Set<RoleResponse> roles;

    private String firstName;

    private String lastName;

    private String username;
}
